package com.jacobcorp.CorpEmployeeRestAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorpEmployeeRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CorpEmployeeRestApiApplication.class, args);
	}

}
